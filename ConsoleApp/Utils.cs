﻿// <copyright file="Utils.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ConsoleApp
{
    using System.Text;

    /// <summary>
    /// Utilities class.
    /// </summary>
    public class Utils
    {
        /// <summary>
        /// This method encode a string to base64.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>Base64 string.</returns>
        public static string Encode(string message)
        {
            byte[] encoded = Encoding.Unicode.GetBytes(message);

            // Console.WindowHeight
            Console.WriteLine($"{encoded.GetType().Name} uses {encoded.Length} bytes");
            return Convert.ToBase64String(encoded);
        }

        /// <summary>
        /// This method decode a base64 string
        /// </summary>
        /// <param name="encodeMessage"></param>
        /// <returns>string.</returns>
        public static string Decode(string encodeMessage)
        {
            byte[] decodedBytes = Convert.FromBase64String(encodeMessage);
            return Encoding.Unicode.GetString(decodedBytes);
        }

        /// <summary>
        /// Test method
        /// </summary>
        /// <param name="message"></param>
        /// <returns>string.</returns>
        public static string TestString(string message)
        {
            return "test";
        }
    }
}