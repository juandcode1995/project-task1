﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace ConsoleApp
{
    using System.Text;
    using static Console;

    /// <summary>
    /// Main Program.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main Method.
        /// </summary>
        public static void Main()
        {
            // hola
            string message = "ñaño";
            WriteLine($"Original word: {message}");
            string encode = Utils.Encode(message);
            WriteLine("encode: " + encode);
            string decode = Utils.Decode(encode);
            WriteLine($"decode: {decode}");
        }
    }
}