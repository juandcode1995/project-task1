namespace Base64Converter.API
{
    using log4net;
    using log4net.Config;
    using Microsoft.Extensions.DependencyInjection.Extensions;
    using static System.Console;

    /// <summary>
    /// Program class.
    /// </summary>
    public class Program
    {
        // private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        static void Main(string[] args)
        {
            WriteLine(typeof(Program));

            // BasicConfigurator.Configure();

            // WriteLine(new FileInfo("log4net.config"));

            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddControllers();

            // Add Scope logger
            builder.Services.AddScoped(factory => LogManager.GetLogger(factory.GetType()));
            var fileLog = builder.Configuration.GetSection("FileLog").Value!;
            XmlConfigurator.Configure(new FileInfo(fileLog));

            // log.Info("Application started!");

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            app.UseSwagger();
            app.UseSwaggerUI();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}