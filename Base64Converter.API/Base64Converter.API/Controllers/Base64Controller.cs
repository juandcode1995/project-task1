﻿namespace Base64Converter.API.Controllers
{
    using System.Text;
    using log4net;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Base64 Converter.
    /// </summary>
    [ApiController]
    [Route("api/v1/encryption/[controller]")]
    public class Base64Controller : Controller
    {
        // private static readonly ILog log = LogManager.GetLogger(typeof(Base64Controller));
        private readonly ILog log;

        /// <summary>
        /// Initializes a new instance of the <see cref="Base64Controller"/> class.
        /// </summary>
        /// <param name="log"></param>
        public Base64Controller(ILog log)
        {
            this.log = log;
        }

        /// <summary>
        /// encrypt base64.
        /// </summary>
        /// <param name="message"></param>
        /// <returns>base64 string</returns>
        [Route("encrypt/{message}")]
        [HttpGet]
        public IActionResult Encode(string message)
        {
            try
            {
                byte[] encoded = Encoding.Unicode.GetBytes(message);
                Console.WriteLine($"{encoded.GetType().Name} uses {encoded.Length} bytes");
                string base64String = Convert.ToBase64String(encoded);
                log.Info($"string {message} encoded to {base64String}");
                return Ok(base64String);
            }
            catch (Exception e)
            {
                log.Error($"Error in encode {message} Exception {e.Message}");
                return BadRequest($"Error in encode {message}");
            }
        }

        /// <summary>
        /// decode method.
        /// </summary>
        /// <param name="encodeMessage"></param>
        /// <returns>string.</returns>
        [Route("decrypt/{encodeMessage}")]
        [HttpGet]
        public IActionResult Decode(string encodeMessage)
        {
            try
            {
                byte[] decodedBytes = Convert.FromBase64String(encodeMessage);
                string decodeMessage = Encoding.Unicode.GetString(decodedBytes);
                log.Info($"string {encodeMessage} decoded to {decodeMessage}");
                return Ok(decodeMessage);
            }
            catch (Exception e)
            {
                log.Error($"Error in decode {encodeMessage} Exception {e.Message}");
                return BadRequest($"Error in decode {encodeMessage}");
            }
        }
    }
}