namespace Base64Converter.API
{
    /// <summary>
    /// Weather class.
    /// </summary>
    public class WeatherForecast
    {
        /// <summary>
        /// Date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Temp.
        /// </summary>
        public int TemperatureC { get; set; }

        /// <summary>
        /// Temp.2
        /// </summary>
        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        /// <summary>
        /// Summary.
        /// </summary>
        public string? Summary { get; set; }
    }
}