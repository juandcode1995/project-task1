"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var data = fs.readFileSync(path.join(__dirname,'../../reports/coverage.cobertura.xml'), { encoding: 'utf-8' });
var find = data.match(/line-rate="[0-1](\.[0-9]+){0,1}"/);
var percentajePassed = parseFloat(find[0].split("\"")[1]);
if (percentajePassed < 0.9)
    throw "Insuficient unit test passed only ".concat(percentajePassed, "!");
//# sourceMappingURL=index.js.map
console.log("All OK");