﻿using System.Text;
using Microsoft.AspNetCore.Mvc;

namespace DockerBase64Converter.Controllers
{
    [ApiController]
    [Route("api/v1/encryption/[controller]")]
    public class Base64Controller : Controller
    {
   

        [Route("encrypt/{message}")]
        [HttpGet]
        public IActionResult Encode(string message)
        {
            byte[] encoded = Encoding.Unicode.GetBytes(message);
            Console.WriteLine($"{encoded.GetType().Name} uses {encoded.Length} bytes");
            string base64String = Convert.ToBase64String(encoded);
            return Ok(base64String);
        }

        [Route("decrypt/{encodeMessage}")]
        [HttpGet]
        public IActionResult Decode(string encodeMessage)
        {
            byte[] decodedBytes = Convert.FromBase64String(encodeMessage);
            string decodeMessage = Encoding.Unicode.GetString(decodedBytes);
            return Ok(decodeMessage);
        }
    }
}