// <copyright file="UnitTest1.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace TestTask1
{
    using ConsoleApp;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// This property always returns a value &lt; 1.
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// This method test the Main.
        /// </summary>
        [TestMethod]
        public void Main()
        {
            // asd
            Program.Main();
        }

        /// <summary>
        /// This method test the encode.
        /// </summary>
        [TestMethod]
        public void Encode()
        {
            var encodedMessage = "8QBhAPEAbwA=";
            var resultEncode = Utils.Encode("ñaño");
            Assert.AreEqual(encodedMessage, resultEncode);
        }

        /// <summary>
        /// This method test the not equal.
        /// </summary>
        [TestMethod]
        public void EncodeNotEqual()
        {
            var encodedMessage = "5THsHg56=rGH";
            var resultEncode = Utils.Encode("�a�o");
            Assert.AreNotEqual(encodedMessage, resultEncode);
        }

        /// <summary>
        /// This method test the decode.
        /// </summary>
        [TestMethod]
        public void Decode()
        {
            var decodeMessage = "ñaño";
            var resultEncode = Utils.Decode("8QBhAPEAbwA=");
            Assert.AreEqual(decodeMessage, resultEncode);
        }

        /// <summary>
        /// This method test the decode not equal.
        /// </summary>
        [TestMethod]
        public void DecodeNotEqual()
        {
            var decodeMessage = "dfas";
            var resultEncode = Utils.Decode("vACAAZQBzAHQAYQBzACAAeQB");
            Assert.AreNotEqual(decodeMessage, resultEncode);
        }
    }
}