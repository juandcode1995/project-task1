FROM mcr.microsoft.com/dotnet/sdk:6.0
COPY release/app/ App/
WORKDIR /App
ENTRYPOINT ["dotnet", "ConsoleApp.dll"]